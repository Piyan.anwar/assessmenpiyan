package router

import (
	"goapi/controller"

	"github.com/gorilla/mux"
)

func Routers() *mux.Router{
	rute:=mux.NewRouter()
	publicrute:=rute.PathPrefix("/api").Subrouter()
	publicrute.HandleFunc("/input-harga",controller.InsertHarga).Methods("POST")
	publicrute.HandleFunc("/check-harga",controller.CekHarga).Methods("GET")
	publicrute.HandleFunc("/topup",controller.TopUp).Methods("POST")
	publicrute.HandleFunc("/saldo",controller.CekSaldoRekening).Methods("POST")
	publicrute.HandleFunc("/buyback",controller.Buyback).Methods("POST")
	publicrute.HandleFunc("/mutasi",controller.GetMutasi).Methods("POST")
	return rute
}