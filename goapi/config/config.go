package config

import (
	"database/sql"
	"fmt"
	"os"
	
	_ "github.com/lib/pq"
	"github.com/joho/godotenv"
)

func ConnDB() *sql.DB {
	err := godotenv.Load(".env")
	if err != nil {
		fmt.Println("Error ENV:", err)
	}

	db, err := sql.Open("postgres", os.Getenv("POSTGRES_URL"))
	if err != nil {
		fmt.Println("Error Database:", err)
	}
	err = db.Ping()
	if err != nil {
		fmt.Println("Koneksi DB Error:", err)
	}
	return db
}
