package controller

import (
	"encoding/json"
	"fmt"
	"goapi/models"
	"net/http"

	"github.com/teris-io/shortid"
)

type responBerhasil struct {
	Res    string `json:"error"`
	Ref_id string `json:"reff_id"`
}
type responGagal struct {
	Res     string `json:"error"`
	Ref_id  string `json:"reff_id"`
	Message string `json:"message"`
}

func InsertHarga(w http.ResponseWriter, r *http.Request) {
	var hargainput models.UpdHarga
	var res string
	refid, _ := shortid.Generate()
	err := json.NewDecoder(r.Body).Decode(&hargainput)
	if err != nil {
		res = "true"
		fmt.Println("error con:", err)
	}
	result := responBerhasil{}
	result2 := responGagal{}
	data := models.InsertHarga(hargainput)
	if data == "true" {
		res = "true"
		result2 = responGagal{
			Res:     res,
			Ref_id:  refid,
			Message: "Bukan admin/data kosong",
		}
		json.NewEncoder(w).Encode(result2)
	} else {
		res = "false"
		result = responBerhasil{
			Res:    res,
			Ref_id: refid,
		}
		json.NewEncoder(w).Encode(result)
	}

}

func CekHarga(w http.ResponseWriter, r *http.Request) {
	cekHarga, _ := json.MarshalIndent(models.CekHarga(), "", "\t")
	w.Write(cekHarga)
}
