package controller

import (
	"encoding/json"
	"fmt"
	"goapi/models"
	"net/http"

	"github.com/teris-io/shortid"
)

func Buyback(w http.ResponseWriter, r *http.Request) {
	var dataBuy models.DataBuyback
	var res string
	refid, _ := shortid.Generate()
	err := json.NewDecoder(r.Body).Decode(&dataBuy)
	if err != nil {
		res = "true"
		fmt.Println("buybackErr:", err)
	}
	
	result := models.Buyback(dataBuy)
	if result == "true" {
		res = "true"
		responf := responGagal{
			Res:     res,
			Ref_id:  refid,
			Message: "Buyback Gagal",
		}
		json.NewEncoder(w).Encode(responf)
	} else {
		res = "false"
		respons := responBerhasil{
			Res:    res,
			Ref_id: refid,
		}
		json.NewEncoder(w).Encode(respons)
	}

}