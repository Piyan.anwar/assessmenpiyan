package controller

import (
	"encoding/json"
	"fmt"
	"goapi/models"
	"net/http"

	"github.com/teris-io/shortid"
)

func GetMutasi(w http.ResponseWriter,r *http.Request){
	var data models.GetTransaksi
	refid,_:=shortid.Generate()
	err:=json.NewDecoder(r.Body).Decode(&data)
	if err!=nil{
		fmt.Println("errMutasi:",err)
	}
	result:=models.GetMutasi(data)
	if result.Res=="true"{
		responf:=responGagal{
			Res: "True",
			Ref_id: refid,
		}
		json.NewEncoder(w).Encode(responf)
	} else {
		json.NewEncoder(w).Encode(result)
	}
}