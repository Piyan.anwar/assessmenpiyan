package controller

import (
	"encoding/json"
	"fmt"
	"goapi/models"
	"net/http"

	"github.com/teris-io/shortid"
)

func TopUp(w http.ResponseWriter, r *http.Request) {
	var datatop models.DataTopup
	var res string
	refid, _ := shortid.Generate()
	err := json.NewDecoder(r.Body).Decode(&datatop)
	if err != nil {
		res = "true"
		fmt.Println("TopupErr:", err)
	}
	respons := responBerhasil{}
	responf := responGagal{}
	result := models.TopUp(datatop)
	if result == "true" {
		res = "true"
		responf=responGagal{
			Res: res,
			Ref_id: refid,
			Message: "Topup Gagal",
		}
		json.NewEncoder(w).Encode(responf)
	}else{
		res="false"
		respons=responBerhasil{
			Res: res,
			Ref_id: refid,
		}
		json.NewEncoder(w).Encode(respons)
	}

}
