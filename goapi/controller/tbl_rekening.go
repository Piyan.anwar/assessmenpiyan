package controller

import (
	"encoding/json"
	"fmt"
	"goapi/models"
	"net/http"

	"github.com/teris-io/shortid"
)

type rekening struct{
	Norek string `json:"norek"`
}

func CekSaldoRekening(w http.ResponseWriter,r *http.Request){
	var rek rekening
	rfid,_:=shortid.Generate()
	err:=json.NewDecoder(r.Body).Decode(&rek)
	if err!=nil{
		fmt.Println("errcekConn:",err)
	}
	responf:=responGagal{}
	result:=models.SaldoRekening(rek.Norek)
	if result.Saldo.Saldo!=0{
		json.NewEncoder(w).Encode(result)
	} else {
		responf=responGagal{
			Res: "true",
			Ref_id: rfid,
			Message: "Error",
		}
		json.NewEncoder(w).Encode(responf)
	}
}