package main

import (
	"goapi/router"
	"log"
	"net/http"

	"github.com/gorilla/handlers"
)

func main() {
	r := router.Routers()
	headers := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	methods := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE"})
	origins := handlers.AllowedOrigins([]string{"*"})
	log.Fatal(http.ListenAndServe(":80", handlers.CORS(headers, methods, origins)(r)))
}