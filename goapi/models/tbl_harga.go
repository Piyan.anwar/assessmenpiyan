package models

import (
	"fmt"
	"goapi/config"
)

type UpdHarga struct {
	Admin_id string `json:"admin_id"`
	Harga    int    `json:"harga_topup"`
	Buyback  int    `json:"harga_buyback"`
}

type Harga struct{
	Buyback  int    `json:"harga_buyback"`
	Harga    int    `json:"harga_topup"`
}

type ResponCekHarga struct{
	Res string `json:"error"`
	Data Harga `json:"data"`
}


func InsertHarga(data UpdHarga) string {
	var res string
	db := config.ConnDB()
	defer db.Close()
	id := data.Admin_id
	if id == "a001" && id != "" {
		sql := "update tbl_harga set topup=$1, buyback=$2"
		_, err := db.Exec(sql, data.Harga, data.Buyback)
		if err != nil {
			res = "true"
			fmt.Println("QueryInsertErr:", err)
		} else {
			res = "false"
		}
	} else {
		res = "true"
	}
	return res
}

func CekHarga()ResponCekHarga{
	var dataharga ResponCekHarga
	db:=config.ConnDB()
	defer db.Close()
	sql:="select topup,buyback from tbl_harga"
	row,err:=db.Query(sql)
	if err!=nil{
		fmt.Println("QCekErr:",err)
	}
	defer row.Close()
	for row.Next(){
		dataharga=ResponCekHarga{
			Res: "False",
		}
		row.Scan(&dataharga.Data.Harga,&dataharga.Data.Buyback)
	}
	return dataharga
}
