package models

import (
	"fmt"
	"goapi/config"
)

type DataBuyback struct {
	Gram  float64 `json:"gram"`
	Harga int     `json:"harga"`
	Norek string  `json:"norek"`
}

func Buyback(data DataBuyback) string {
	var res string
	saldoNow := GetSaldo(data.Norek)
	db := config.ConnDB()
	defer db.Close()
	// fmt.Println(data.Gram)
	// fmt.Println(data.Harga)
	// fmt.Println(data.Norek)
	// fmt.Println(saldoNow)
	saldoBerkurang:=data.Gram
	// fmt.Println(saldoBerkurang)
	if data.Norek == "r001" && data.Norek != "" && saldoNow > saldoBerkurang {
		sum := saldoNow - saldoBerkurang
		// fmt.Println(sum)
		UpdSaldo(sum)
		sql := "insert into tbl_buyback (gram,harga,norek_id)values($1,$2,$3)"
		_, err := db.Exec(sql, data.Gram, data.Harga, data.Norek)
		if err != nil {
			res = "true"
			fmt.Println("BuybackErr:", err)
		} else {
			res = "false"
		}
		tanggal:=makeTimestamp()
		RecordTransaksi(data.Norek,tanggal,"buyback",saldoBerkurang,sum)
	} else {
		res = "true"
	}
	return res
}
