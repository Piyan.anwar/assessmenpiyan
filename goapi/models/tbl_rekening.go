package models

import (
	"fmt"
	"goapi/config"
)

type Saldo struct {
	Norek string  `json:"norek"`
	Saldo float32 `json:"saldo"`
}

type ResponCekSaldo struct {
	Res   string `json:"error"`
	Saldo Saldo  `json:"data"`
}

func SaldoRekening(norek string) ResponCekSaldo {
	var respon ResponCekSaldo
	var res string
	db := config.ConnDB()
	defer db.Close()
	sql := "select norek,saldo from tbl_rekening where norek=$1"
	row, err := db.Query(sql,norek)
	if err != nil {
		res = "true"
		fmt.Println("errorSaldo:", err)
	} else {
		res = "false"
	}
	defer row.Close()
	respon = ResponCekSaldo{
		Res: res,
	}
	for row.Next() {
		row.Scan(&respon.Saldo.Norek, &respon.Saldo.Saldo)
	}
	return respon
}
