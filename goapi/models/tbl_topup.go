package models

import (
	"fmt"
	"goapi/config"
	"math"
	"strconv"
)

type DataTopup struct {
	Gram  string `json:"gram"`
	Harga string `json:"harga"`
	Norek string `json:"norek"`
}

func roundFloat(val float64, precision uint) float64 {
	ratio := math.Pow(10, float64(precision))
	return math.Round(val*ratio) / ratio
}

func TopUp(data DataTopup) string {
	db := config.ConnDB()
	var res string
	saldoNow := GetHargaNow()
	gramConv, _ := strconv.ParseFloat(data.Gram, 32)
	// fmt.Println(gramConv)
	gramRound := roundFloat(gramConv, 3)
	// fmt.Println(gramRound)
	hargaConv, _ := strconv.Atoi(data.Harga)
	defer db.Close()
	if data.Norek == "r001" && data.Norek != "" && saldoNow == hargaConv && gramRound > 0.001 {
		saldo := GetSaldo(data.Norek)
		sum := saldo + gramRound
		sumBulat:=roundFloat(sum,3)
		// fmt.Println(sumBulat)
		UpdSaldo(sumBulat)
		sql := "insert into tbl_topup (gram,harga,norek_id) values($1,$2,$3)"
		_, err := db.Exec(sql, gramRound, hargaConv, data.Norek)
		if err != nil {
			res = "true"
			fmt.Println("TopupErr:", err)
		} else {
			res = "false"
		}
		tanggal:=makeTimestamp()
		RecordTransaksi(data.Norek,tanggal,"topup",gramRound,sumBulat)
	} else {
		res = "true"
	}
	return res
}

func UpdSaldo(saldo float64) {
	db := config.ConnDB()
	defer db.Close()
	sql := "update tbl_rekening set saldo=$1"
	_, err := db.Exec(sql, saldo)
	if err != nil {
		fmt.Println("updateRekErr:", err)
	}
}

func GetSaldo(norek string) float64 {
	var saldo float64
	db := config.ConnDB()
	defer db.Close()
	sql := "select saldo from tbl_rekening where norek=$1"
	row, err := db.Query(sql, norek)
	if err != nil {
		fmt.Println("errorGetSaldo:", err)
	}
	defer row.Close()
	for row.Next() {
		row.Scan(&saldo)
	}
	// fmt.Println("gs", saldo)
	return saldo
}

func GetHargaNow() int {
	var data int
	db := config.ConnDB()
	defer db.Close()
	sql := "select topup from tbl_harga"
	row, err := db.Query(sql)
	if err != nil {
		fmt.Println("getnowhargaerr:", err)
	}
	defer row.Close()
	for row.Next() {
		row.Scan(&data)
	}
	return data
}
