package models

import (
	"fmt"
	"goapi/config"
	"time"
)

type GetTransaksi struct {
	Norek     string `json:"norek"`
	DateStart int    `json:"start_date"`
	DateEnd   int    `json:"end_date"`
}

type Transaksi struct {
	Tanggal int64 `json:"date"`
	Tipe    string	`json:"type"`
	Gram    float64 `json:"gram"`
	Harga   int `json:"harga-topup"`
	Buyback int `json:"harga-buyback"`
	Saldo   float64 `json:"saldo"`
}

type ResTransaksi struct {
	Res  string      `json:"error"`
	Data []Transaksi `json:"data"`
}

func makeTimestamp() int64 {
    return time.Now().UnixNano() / int64(time.Millisecond)
}

func GetMutasi(data GetTransaksi)ResTransaksi{
	var result ResTransaksi
	var dataTr []Transaksi
	var msg string
	db:=config.ConnDB()
	defer db.Close()
	sql:="select date,type,gram,topup,buyback,saldo from tbl_transaksi t join tbl_harga h on h.id=t.harga_id where t.norek_id=$1 and date between $2 and $3"
	row,err:=db.Query(sql,data.Norek,data.DateStart,data.DateEnd)
	if err!=nil{
		msg="true"
		fmt.Println("MutasiErr:",err)
	} else {
		msg="false"
	}
	defer row.Close()
	for row.Next(){
		var rows Transaksi
		err=row.Scan(&rows.Tanggal,&rows.Tipe,&rows.Gram,&rows.Harga,&rows.Buyback,&rows.Saldo)
		if err!=nil{
			fmt.Println("errScan:",err)
		}
		dataTr=append(dataTr, rows)
	}
	result=ResTransaksi{
		Res: msg,
		Data: dataTr,
	}
	return result
}

func RecordTransaksi(norek string,dates int64,tipe string,gram float64,saldo float64){
	db := config.ConnDB()
	defer db.Close()
	sql:="Insert into tbl_transaksi(norek_id,date,type,gram,saldo) values ($1,$2,$3,$4,$5)"
	_,err:=db.Exec(sql,norek,dates,tipe,gram,saldo)
	if err!=nil{
		fmt.Println("RecordT:",err)
	}
}